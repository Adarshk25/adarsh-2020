-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sql_mock_test
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author details`
--

DROP TABLE IF EXISTS `author details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author details` (
  `Author ID` int NOT NULL AUTO_INCREMENT,
  `Author name` varchar(45) NOT NULL,
  `author_dob` date NOT NULL,
  `description` varchar(45) NOT NULL,
  `created_on` date NOT NULL,
  `modified_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `staus` int NOT NULL,
  PRIMARY KEY (`Author ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author details`
--

LOCK TABLES `author details` WRITE;
/*!40000 ALTER TABLE `author details` DISABLE KEYS */;
/*!40000 ALTER TABLE `author details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book details`
--

DROP TABLE IF EXISTS `book details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book details` (
  `book ID` int NOT NULL AUTO_INCREMENT,
  `book title` varchar(45) NOT NULL,
  `book author` varchar(45) NOT NULL,
  `book  publisher` varchar(45) NOT NULL,
  `book genre` varchar(45) NOT NULL,
  `year of release` date NOT NULL,
  `rating` int NOT NULL,
  `created_on` date NOT NULL,
  `modified_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `staus` int NOT NULL,
  PRIMARY KEY (`book ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book details`
--

LOCK TABLES `book details` WRITE;
/*!40000 ALTER TABLE `book details` DISABLE KEYS */;
/*!40000 ALTER TABLE `book details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher details`
--

DROP TABLE IF EXISTS `publisher details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publisher details` (
  `publiser ID` int NOT NULL AUTO_INCREMENT,
  `publisher name` varchar(45) NOT NULL,
  `established on` date NOT NULL,
  `address` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `created_on` date NOT NULL,
  `modified_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`publiser ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher details`
--

LOCK TABLES `publisher details` WRITE;
/*!40000 ALTER TABLE `publisher details` DISABLE KEYS */;
/*!40000 ALTER TABLE `publisher details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user registration`
--

DROP TABLE IF EXISTS `user registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user registration` (
  `Reg ID` int NOT NULL AUTO_INCREMENT,
  `First name` varchar(25) NOT NULL,
  `Last namel` varchar(25) DEFAULT NULL,
  `Gender` varchar(10) NOT NULL,
  `Date of birth` date NOT NULL,
  `Normal / Premium user` varchar(10) NOT NULL,
  `Address` varchar(45) NOT NULL,
  `Phone Number` int NOT NULL,
  `created_on` date NOT NULL,
  `modified_on` date NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`Reg ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user registration`
--

LOCK TABLES `user registration` WRITE;
/*!40000 ALTER TABLE `user registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `user registration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 12:14:47
