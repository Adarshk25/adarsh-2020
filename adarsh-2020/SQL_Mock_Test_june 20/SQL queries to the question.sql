-- Que 1
INSERT INTO `sql_mock_test`.`author details` (`Author ID`, `Author name`, `author_dob`, `description`, `created_on`, `modified_on`, `created_by`, `modified_by`, `staus`) VALUES ('1', 'Mark Twain', '1847-09-02', 'American Author', '2020-06-05', '2020-06-07', 'admin', 'admin', '1');

-- Que 2
INSERT INTO `sql_mock_test`.`publisher details` (`publiser ID`, `publisher name`, `established on`, `address`, `description`, `created_on`, `modified_on`, `created_by`, `modified_by`, `status`) VALUES ('1', 'Hachatte Livre', '1990-01-01', 'UK', 'American based publisher', '2016-01-01', '2017-02-05', 'admin', 'admin', '0');
INSERT INTO `sql_mock_test`.`publisher details` (`publiser ID`, `publisher name`, `established on`, `address`, `description`, `created_on`, `modified_on`, `created_by`, `modified_by`, `status`) VALUES ('2', 'Harrper collind', '1890-01-02', 'UK', 'American publishers', '2017-05-05', '2020-06-05', 'admin', 'admin', '1');

-- Que 3
INSERT INTO `sql_mock_test`.`book details` (`book genre`) VALUES ('Fictional');
INSERT INTO `sql_mock_test`.`book details` (`book genre`) VALUES ('Non-Fictional');
INSERT INTO `sql_mock_test`.`book details` (`book genre`) VALUES ('Mystery');
INSERT INTO `sql_mock_test`.`book details` (`book genre`) VALUES ('Thriller');
INSERT INTO `sql_mock_test`.`book details` (`book genre`) VALUES ('Narrative');

-- Que 4
INSERT INTO `sql_mock_test`.`book details` (`book ID`, `book title`, `book author`, `book  publisher`, `book genre`, `year of release`, `rating`, `created_on`, `modified_on`, `created_by`, `modified_by`, `staus`) VALUES ('1', 'Jane Eyre', 'Charlotte Bronte', 'Hattache Livre', 'fictional', '1990', '5', '2000-01-05', '2016-06-20', 'admin', 'admin', '1');

-- Que 5
INSERT INTO `sql_mock_test`.`user registration` (`Reg ID`, `First name`, `Last namel`, `Gender`, `Date of birth`, `Normal / Premium user`, `Address`, `Phone Number`, `created_on`, `modified_on`, `created_by`, `modified_by`, `status`) VALUES ('1', 'Raju', 'Sinha', 'Male', '25-05-1997', 'premium', 'KP-9B', '8457682822', '2019-05-05', '2020-05-06', 'admin', 'admin', '0');

-- Que 6
SELECT `book details`.`book title`, `author details`.`author name`
FROM `book details`
JOIN `author details` ON `book details`.`Book ID` = `author details`.`Author ID`
WHERE `book details`.`book title` = 'X' AND `author details`.`author name` = 'Y';

-- Que 10
SELECT * FROM `user registration` WHERE `Normal/Premium user` != 'Premium';

-- Que 11
SELECT * FROM `user registration` WHERE `Gender` != 'Female';

-- Que 12
SELECT `book genre` FROM `book details` GROUP BY `book genre`;

-- Que 13
SELECT `book title` FROM `book details` WHERE rating > 4;

-- Que 14
SELECT `book title` FROM `book details` WHERE rating = MAX(rating);

-- Que 15
SELECT `book title` FROM `book details` WHERE rating = MIN(rating);

-- Que 16
SELECT *
FROM `author details`
WHERE `Author name` LIKE 'AR%';

-- Que 17
SELECT * from `publisher details` WHERE `established on` > 2012;

-- Que 18
SELECT * FROM `book details` WHERE `book ID` <= 5 (SELECT `book title` FROM `book details` ORDER BY rating DESC);