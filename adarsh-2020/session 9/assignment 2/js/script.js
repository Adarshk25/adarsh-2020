var row = prompt("Enter odd number of rows");

var i, j, k, a;

if (row % 2 != 0) {
  for (i = 1; i <= row; i++) {
    a = 1;
    for (j = i; j <= row; j++) {
      document.write("&nbsp");
    }

    for (k = 1; k <= 2 * i - 1; k++) {
      document.write(a++);
    }

    document.write("<br />");
  }
} else {
  document.write("Invalid Input");
}
