$(document).ready(function() {
    $('.form_error').hide();
    $('#subSubmit').click(function(){
         var firstName = $('#txtFirstName').val();
         var lastName = $('#txtlastName').val();
         var email = $('#emlEmail').val();
         var phone = $('#numContactNumber').val();
         var make = $('#ddlMake').val();
         var color = $('#ddlcolor').val();
         var textArea = $('#txtArea').val();
         if(firstName== ''){
            $('#name').next().show();
            return false;
          }
          if(lastName== ''){
              $('#name').next().show();
              return false;
            }
          if(email== ''){
             $('#email').next().show();
             return false;
          }
          if(IsEmail(email)==false){
              $('#invalid_email').show();
              return false;
          }

          if(phone== ''){
              $('#phone').next().show();
              return false;
          }
          if(make== ''){
              $('#ddlMake').next().show();
              return false;
          }
          if(color== ''){
              $('#ddlColor').next().show();
              return false;
          }
          if(textArea== ''){
              $('#txtArea').next().show();
              return false;
          }
    });
    function IsEmail(email) {
      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if(!regex.test(email)) {
         return false;
      }else{
         return true;
      }
    }
  });
