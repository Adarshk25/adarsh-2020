var friendsBirthday = {
     "friends" : [
        {
            name : 'friend1',
            dob : '07-13-1998'
        },
    
        {
            name : 'friend2',
            dob : '09-16-1998'
        },
    
        {
            name : 'friend3',
            dob : '09-17-1998'
        },
    
        {
            name : 'friend4',
            dob : '09-16-1995'
        },
    
        {
            name : 'friend5',
            dob : '09-17-1995'
        },
    
        {
            name : 'friend6',
            dob : '07-16-1997'
        },
    
        {
            name : 'friend7',
            dob : '09-15-1998'
        },
    
        {
            name : 'friend8',
            dob : '08-16-1999'
        },
    
        {
            name : 'friend9',
            dob : '07-15-1995'
        },
    
        {
            name : 'friend10',
            dob : '05-17-1998'
        },
    ] 
}


localStorage.setItem("friendsBirthday", JSON.stringify(friendsBirthday));

function friendsNextMonthBday() {
    for(month in friendsBirthday) {
        friendsBirthday.friends.forEach(element => {
          var todayDate = new Date();
          var dateFormat = new Date(element.dob);
          var monthOfTodayDate = todayDate.getMonth();
          var nextMonth = dateFormat.getMonth();
          if( nextMonth-monthOfTodayDate === 1) {
              console.log(`${element.name} has birthday in the next month`);
            }
       });
    }
}

friendsNextMonthBday();